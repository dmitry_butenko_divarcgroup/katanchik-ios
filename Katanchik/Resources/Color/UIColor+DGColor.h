//
//  UIColor+DGColor.h
//  Katanchik
//
//  Created by divarc on 28.01.16.
//  Copyright © 2016 divarcgroup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (DGColor)

+ (UIColor *)DGColorStyle_Pink;

@end
