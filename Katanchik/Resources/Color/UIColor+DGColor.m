//
//  UIColor+DGColor.m
//  Katanchik
//
//  Created by divarc on 28.01.16.
//  Copyright © 2016 divarcgroup. All rights reserved.
//

#import "UIColor+DGColor.h"

@implementation UIColor (DGColor)

+ (UIColor *)DGColorStyle_Pink {
    return [self colorFromHexString:@"#bf85e4"];
}

#pragma mark - Private

+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1];
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0
                           green:((rgbValue & 0xFF00) >> 8)/255.0
                            blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

@end
