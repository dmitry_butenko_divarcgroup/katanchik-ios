#import "Storyboard Constants.h"

@implementation DGLoginViewController ( StoryboardIdentifiers )

+(instancetype)controllerLOGIN {
   UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Auth" bundle:nil];
   return [storyboard instantiateViewControllerWithIdentifier:@"LOGIN"];
}
@end



@implementation DGSignUpViewController ( StoryboardIdentifiers )

+(instancetype)controllerSIGN_UP {
   UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Auth" bundle:nil];
   return [storyboard instantiateViewControllerWithIdentifier:@"SIGN_UP"];
}
@end

