#import <UIKit/UIKit.h>



#import "DGLoginViewController.h"



@interface DGLoginViewController ( StoryboardIdentifiers )

+(instancetype)controllerLOGIN;

@end


#import "DGSignUpViewController.h"



@interface DGSignUpViewController ( StoryboardIdentifiers )

+(instancetype)controllerSIGN_UP;

@end
