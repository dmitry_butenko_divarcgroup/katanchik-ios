#import "AuthSegue.h"

@implementation UIStoryboard (Auth)

+ (UIViewController *)initializeInitialViewCotrollerFromAuth
{
   UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Auth" bundle:nil];

   return (UIViewController *)[storyboard instantiateInitialViewController];
}

@end


@implementation DGSignUpViewController (Auth)

+ (instancetype)controllerSIGN_UP_NOW
{
   UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Auth" bundle:nil];

   return [storyboard instantiateViewControllerWithIdentifier:@"SIGN_UP_NOW"];
}

@end

