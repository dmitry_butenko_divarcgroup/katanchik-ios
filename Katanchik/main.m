//
//  main.m
//  Katanchik
//
//  Created by divarc on 28.01.16.
//  Copyright © 2016 divarcgroup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DGAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DGAppDelegate class]));
    }
}
