//
//  DGAppDelegate.h
//  Katanchik
//
//  Created by divarc on 28.01.16.
//  Copyright © 2016 divarcgroup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
